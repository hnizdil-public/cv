FROM ubuntu:latest

RUN apt-get -qq update \
	&& apt-get -y -q install \
		fontconfig \
		libfontconfig1 \
		make \
		pandoc \
		perl \
		wget

WORKDIR /texlive

COPY texlive.profile .

# http://tug.org/texlive/acquire-netinstall.html
RUN wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz -O- | tar zxf - \
	&& ./install-tl-*/install-tl --profile texlive.profile \
	&& rm -r install-tl-* \
	&& /usr/local/texlive/*/bin/*/tlmgr path add

RUN tlmgr install \
	catchfile \
	datetime2 \
	enumitem \
	environ \
	hardwrap \
	hyperref \
	lastpage \
	libertine \
	marginnote \
	pagecolor \
	polyglossia \
	sectsty \
	tcolorbox \
	tikzfill \
	wallpaper

# https://tug.org/texlive/doc/texlive-en/texlive-en.html#x1-330003.4.4
RUN cp /usr/local/texlive/*/texmf-var/fonts/conf/texlive-fontconfig.conf /etc/fonts/conf.d/09-texlive.conf \
    && fc-cache -fsv

ENTRYPOINT ["make"]
